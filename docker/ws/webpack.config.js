const path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: ['./src/index.tsx'],
        vendor: ['react', 'react-dom']
    },
    output: {
        path: path.resolve(__dirname, 'server/assets/'),
        filename: 'js/[name].bundle.js'
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
              test: /\.(js|jsx|tsx|ts)$/,
              exclude: /node_modules/,
              loader: [ 'babel-loader', 'awesome-typescript-loader']
            },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    plugins: [
        // new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'src', 'app', 'index.html') }),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
      before: function(app, server) {
        app.get('/server/assets/css/style.css', function(req, res) {
          res.sendFile(path.join(__dirname, './server/assets/css/', 'style.css'))
        });
      },
      contentBase: path.join(__dirname, 'dev'),
      compress: true,
      historyApiFallback: true,
      port: 9000
    }
}