import * as React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Counter from "./components/Counter/Counter";
import History from "./components/Counter/History";

import Page from '../src/Page';

  function Index() {
    return <h2>Home</h2>;
  }

  function Product({ match }) {
    return <h2>This is a page for product with ID: {match.params.id} </h2>;
  }
 
  function AppRouter() {
    return (
     <Router>
       <div>
         <nav>
           <ul>
             <li>
               <Link to="/">Home</Link>
             </li>
             <li>
               <Link to="/products/1">First Product</Link>
             </li>
             <li>
               <Link to="/products/2">Second Product</Link>
             </li>
             <li>
               <Link to="/test">Page (Todo App)</Link>
             </li>
             <li>
               <Link to="/counter">Counter</Link>
             </li>
             <li>
               <Link to="/history">History</Link>
             </li>
           </ul>
         </nav>
         <Route path="/" exact component={Index} />
         <Route path="/test" component={Page} />
         <Route path="/products/:id" component={Product} />
         <Route path="/counter" component={Counter} />
         <Route path="/history" component={History} />
       </div>
     </Router>
   );
 }
 
export default AppRouter;