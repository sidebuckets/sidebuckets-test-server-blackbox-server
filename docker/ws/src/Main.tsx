import * as React from 'react';

class Main extends React.Component<{}, { data: any }> {
  
  constructor (props) {
    super(props);
    this.state = {
      data: {}   
    }
  }
  
  componentDidMount () {
      let self = this;
     
		  let	ws = new WebSocket("wss://ws.localhost/echo");
      console.log(ws);

      ws.onopen = function(evt) {
      var cors_api_url = 'https://api.localhost';
      function doCORSRequest(options: any, printResult: any) {
        var x = new XMLHttpRequest();
        x.open(options.method, cors_api_url + options.url);
        x.onload = x.onerror = function() {
          printResult(
            (JSON.parse(x.responseText) || '')
          );
        };
        if (/^POST/i.test(options.method)) {
          x.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
        x.send(options.data);
      }
      doCORSRequest({
        method: 'POST',
        url: '', //leave blank func above handles this..
        data: JSON.stringify({
                "query": "query{\n  user {\n   user_name\n    user_email\n    user_password\n  }\n}",
                "variables": null
              })
      }, function printResult(result: any) {
        console.log(JSON.stringify(result));
        ws.send(result);
        self.setState(result, () => {
        console.log("what is state", self.state);
      })
      });
      ws.onclose = function(evt) {
				console.log('CLOSE', evt)
				// ws = null;
			}
			ws.onmessage = function(evt) {
				console.log('RESPONSE', evt.data)
				// document.getElementById('output').innerHTML = evt.data;
			}
			ws.onerror = function(evt) {
				console.log('ERROR', evt)
			}
    }
  }
  
  render() {
    const { data } = this.state;

    let item = data.user || [];

    return (
      <div className="backgroundColor">
      <div id="output"></div>

          <div>{JSON.stringify(item)}</div> 

          <div>{
            item.map((el, k) => {
              var username = el.user_name;
              var password = el.user_password;
              var email    = el.user_email;
              return <div key={k}> 
                <h1>Username :</h1> {username}
                <h1>Password :</h1> {password}
                <h1>Email :</h1> {email}
              </div>
            })}</div>
    
        <div className="card-body">
          <h5 className="card-title">Steve Jobs</h5>
          <h6 className="card-subtitle mb-2 text-muted">steve@apple.com</h6>
          <p className="card-text">Stay Hungry, Stay Foolish</p>
        </div>
      </div>
    );
  }
}

export default Main;