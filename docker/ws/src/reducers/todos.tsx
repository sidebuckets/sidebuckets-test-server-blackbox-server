import { ADDTODO, REMOVETODO } from '../actions/todos'

const INITIAL_STATE = ["Test"];

export default function todos(state = INITIAL_STATE, {type, payload}) {
  switch(type) {
    case ADDTODO:
      return state.concat(payload);
    case REMOVETODO: 
      //_ is equal to private variable name or example 'anythingiwant'
      return state.filter((_, i) => i !== payload)
    default:
      return state;
  }
}