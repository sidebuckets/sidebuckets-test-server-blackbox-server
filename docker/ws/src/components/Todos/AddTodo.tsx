import * as React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import { addTodo } from '../../actions/todos';

interface Props {
  value: any,
}

class AddTodo extends React.Component<{ addTodo: (value: string) => void}> {
  state = {
    value:  ""
  }

  render() {    

    const { addTodo } = this.props; 
    const { value } = this.state;

    return ( 
      <div> 
       <form onSubmit={e => {
          e.preventDefault()
          if (value === null || value ===  '' || value === undefined ) {
            console.log('err, user submited null value')
          } else {
            addTodo(value)
          }
        }}>
          <input value={value} onChange={({ target: { value } }) => this.setState({ value })} placeholder="add todo" />
          <input type="submit" />
      </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    value: state.value,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addTodo: addTodo
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);