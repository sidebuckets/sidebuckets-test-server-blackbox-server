import * as React from "react"

export default ({ todo }: { todo: string }) => (
 todo !== null ? <span> {todo} </span> : 
  null 
)
