import * as React from 'react';
import { connect } from "react-redux"
import Todo from "./Todo";
import { removeTodo } from '../../actions/todos';

interface IProps {
    todos: any;
    remove: any;
    // any other props that come into the component
}

const Todos = ({ todos, remove }: IProps) => (
  <ul>
    {todos.map((todo: any, i: any) => (
       todo !== null ?
        <div key={i}>
            <li key={i}>
              <Todo todo={todo} />{" "}
            <span style={{ cursor: "pointer" }} onClick={() => remove(i)}>❌</span>
            </li>
          <br />
        </div>
        : null 
    ))}
  </ul>
)

export default connect(
  ({ todos }: IProps) => ({ todos }),
  dispatch => ({ remove: i => dispatch(removeTodo(i)) })
)(Todos)
