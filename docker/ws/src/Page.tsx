import * as React from 'react';
import Main from "./Main";

class Page extends React.Component {
  render() {
    return (
      <div className="backgroundColor">
        <h1> Helloworld </h1>
        <Main />
      </div>
    );
  }
}

export default Page;