import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import AppRouter from "./AppRouter";
import AddTodo from "./components/Todos/AddTodo";
import Todos from "./components/Todos/Todos";
import reducers from './reducers';

export default () => (
  <Provider store={createStore(reducers)}>
    <AppRouter />
    <AddTodo />
    <Todos />
  </Provider>
);
