import { combineActions } from 'redux-actions';

import { increaseCount, decreaseCount, resetCount } from './counter';
import { addTodo, removeTodo } from './todos';

export default combineActions<any, any>({
  increaseCount, 
  decreaseCount, 
  resetCount,
  addTodo,
  removeTodo
});