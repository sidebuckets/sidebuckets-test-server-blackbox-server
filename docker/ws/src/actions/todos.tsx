export const ADDTODO    = 'ADDTODO';
export const REMOVETODO = 'REMOVETODO';

export function addTodo(todo) {
  return ({ type: ADDTODO, payload: todo !== '' ? todo : null })
}

export function removeTodo(index) {
  return ({ type: REMOVETODO, payload: index })
}