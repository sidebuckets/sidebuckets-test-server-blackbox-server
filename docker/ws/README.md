### React/Redux/Typescript Front End:

Compile and save to `server/assets/js` folder 
 
  ```
   yarn build
  ```

Compile and save to `server/assets/css` folder 
 
  ```
   yarn sass 
  ```

Open in browser to check outputed webpack js locally (chrome)

```
  public/index.html 
```

### FAQ:

## How to start

Run a react front end build process before a docker build process 

  ```
    yarn build && docker-compose build && docker-compose up
  ```

Run full docker and frontend with sass cd `ws` folder 

  ```
    yarn build && yarn sass && docker-compose build && docker-compose up

  ```

## Production 

Change all localhost references to live domain

  ```
    localhost -> domainname 
  ```