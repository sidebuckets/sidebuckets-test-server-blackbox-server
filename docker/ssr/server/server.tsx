import * as React from "react";
import * as ReactDOM from 'react-dom/server';

import App from '../src/components/App';

const { readFileSync } = require('fs')
const Fastify = require('fastify')

const GLOBAL_STATE = {
  text: 'helloworldfromstate',
}

const html = ReactDOM.renderToString(<App />)

const fastify = Fastify({
  logger: { level: 'trace'},
  ignoreTrailingSlash: true,
  maxParamLength: 200,
  caseSensitive: true,
  trustProxy: true,
})

console.log(fastify.initialConfig)
/*
will log :
{
  caseSensitive: true,
  https: { allowHTTP1: true },
  ignoreTrailingSlash: true,
  maxParamLength: 200
}
*/

fastify.register(async (instance:any, opts:any) => {
  instance.get('/', async (request:any, reply:any) => {

  const htmlRender = `
    <!DOCTYPE html>
    <html>
      <head>
        <script>
          GLOBAL_STATE = ${JSON.stringify(GLOBAL_STATE)};
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hello Bulma!</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
      </head>
      <body>
        <div id="root">${html}</div>
      
        
        <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
        <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
        <script src="./main.js"></script>
      </body>
    </html>`;
    reply.type('text/html');
    reply.send(String(htmlRender));
    
    // Webpack External ejects react - load directly from cdn
    // reply.send({hello: 'test'})
    // reply.send({ hello: 'world' }) //example - place html rendered markup from react HERE
    // return instance.initialConfig
    /*
    will return :
    {
      caseSensitive: true,
      https: { allowHTTP1: true },
      ignoreTrailingSlash: true,
      maxParamLength: 200
    }
    */
  })

  instance.get('/error', async (request:any, reply:any) => {
    // will throw an error because initialConfig is read-only
    // and can not be modified
    instance.initialConfig.https.allowHTTP1 = false

    return instance.initialConfig
  })
})

fastify.get('/main.js', function (request:any, reply:any) {
  reply
  .code(200)
  .header('Content-Type', 'text/javascript;charset=UTF-8')
  .send(readFileSync('./dist/main.js'));
})

fastify.get('/main.js.map', function (request:any, reply:any) {
  reply
  .code(200)
  .header('Content-Type', 'text/javascript;charset=UTF-8')
  .send(readFileSync('./dist/main.js.map'));
})

// Start listening.
fastify.listen(8081, '0.0.0.0', (err:any, address:any) => {
  
  console.log(`Listening on ${address}`)
 
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
})