import * as React from "react";
import { Query } from "../utils/ws";

export default class List extends React.Component<{}, { data:any }> {
  constructor (props:any) {
    super(props);
    this.state = {
      data: {}
    }
  }
  componentDidMount () {
    let self = this;
    Query(self, "query{\n  user {\n   user_name\n    user_email\n    user_password\n  }\n}", true);
  }
  public render() {
        const { data } = this.state;
        let item = data.user || [];
        return (
            <React.Fragment>          
                <div>{JSON.stringify(item)}</div> 
                <div>{
                  item.map((el:any, k:any) => {
                    var username = el.user_name;
                    var password = el.user_password;
                    var email    = el.user_email;
                    return <div key={k}> 
                      <h1>Username :</h1> {username}
                      <h1>Password :</h1> {password}
                      <h1>Email :</h1> {email}
                    </div>
                  })}</div>
            </React.Fragment>
        );
   }
} 