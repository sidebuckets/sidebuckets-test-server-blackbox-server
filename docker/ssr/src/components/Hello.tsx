import * as React from "react";

interface CustomInputProps {
  compiler: string;
  framework: string;
}

export default class Hello extends React.Component<CustomInputProps> {
    compiler: any;
    framework: any;

    public render() {
        return (
            <React.Fragment>
                <h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>
            </React.Fragment>
        );
    }
}
