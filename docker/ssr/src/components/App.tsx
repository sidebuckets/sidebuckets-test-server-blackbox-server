import * as React from "react";
import Hello from '../components/Hello';
import List from '../components/List';

export default class App extends React.Component {
    public render() {
        return (
            <div>
              <Hello compiler={'Helloworld'} framework={'Test'} />
              <List />
            </div>
        );
    }
}
