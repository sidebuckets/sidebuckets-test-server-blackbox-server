// saveState only working in Component Did Mount React JS context. 
// IF not use basic query and ignore the rest 
export const Query = (context:any, graphQuery?:any, saveState?:any, postType?:any, apiUrl?:any, websocketUrl?:any) => {
  let	ws = new WebSocket(websocketUrl || "wss://ws.localhost/echo");
    ws.onopen = function(evt) {
      var cors_api_url =  apiUrl || "https://api.localhost";
      function doCORSRequest(options: any, printResult: any) {
        var x = new XMLHttpRequest();
        x.open(options.method, cors_api_url + options.url);
        x.onload = x.onerror = function() {
          printResult(
            (JSON.parse(x.responseText) || '')
          );
        };
        if (/^POST/i.test(options.method)) {
          x.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
        x.send(options.data);
      }
      doCORSRequest({
        method: postType || 'POST',
        url: '', //leave blank func above handles this..
        data: JSON.stringify({"query": graphQuery,"variables": null})
      }, function printResult(result: any) {
       if (saveState === true) { 
        console.log(JSON.stringify(result));
       }
        ws.send(JSON.stringify(result));
      });
      ws.onclose = function(evt) {
        console.log('CLOSE', evt)
      }
      ws.onmessage = function(evt) {
        const result = JSON.parse(evt.data);
        if (saveState === true) {
          context.setState(result, () => {
          console.log("what is state", context.state);
          })
          console.log('RESPONSE', evt.data)
        } 
      }
      ws.onerror = function(evt) {
          console.log('ERROR', evt)
      }
    }
}