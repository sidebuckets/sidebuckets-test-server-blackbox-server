## Getting Started 

```
yarn install
```

```
npx webpack
```

## Build Dist Folder with Webpack (aka - npx webpack) 

```
yarn run build
```

```
yarn start
```

## Generate SSL for HTTP Node Server - Pull from Node JS Docs Directly 

```
openssl req -x509 -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' \
  -keyout localhost-privkey.pem -out localhost-cert.pem

```

## Build Source File 

```
docker build .
```

## Bind to Port 8081 (above 8080 for ssl) 0.0.0.0 will be attached with -p tag  

```
docker run -p 8081:8081 ff8f46bf01d0
```

## Note - The ssr depends on the mysql only for loading sequence. The SSL is handled via python proxy container, removed for server.tsx directly. 