<?php 
namespace BlackBox\Components;

use GraphQL\GraphQL;
use GraphQL\Utils\BuildSchema;

class Graph 
{
  /**
   * Util dataParser 
   */
  public function DataParser($decode) {
    return json_decode($decode);
  }

  /**
   * Starting A GraphQL Json Route with schema file
   */
  public function InitializeGraphQL()
  {
    # Init GraphQl Interface 
    try {
        $schema = BuildSchema::build(file_get_contents(__DIR__ . '/GraphQL/schema.graphqls'));
        $rootValue = require_once __DIR__ . '/Root.php';
        $rawInput = file_get_contents('php://input');
        $input = json_decode($rawInput, true);
        $query = $input['query'];
        $variableValues = isset($input['variables']) ? $input['variables'] : null;
        $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);
    } catch (\Exception $e) {
        $result = [
            'error' => [
                'message' => $e->getMessage()
            ]
        ];
    }
    $whitelist = array(
      '::1:8444',
      'localhost'
    );
    if(in_array($_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['SERVER_PORT'], $whitelist)) {
        // Only On Dev Server ports
        header('Content-Type: application/json; charset=UTF-8'); 
        echo json_encode($result);
    } else {
       header('Content-Type: application/json; charset=UTF-8'); 
       echo json_encode($result);
    }
  }
}
?>