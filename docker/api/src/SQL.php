<?php 
namespace BlackBox\Components;

class SQL {

  public static $user     = '';
  public static $password = '';
  public static $dbName   = '';
  public static $dbhost   = '';
  public static $submit   = [];
  
  private function dbAccess() {
    DB::$user     = 'user';
    DB::$password = 'password';
    DB::$dbName   = 'db';
    DB::$host     = 'mysql:3306';
  }
 
  //SQL COMMAND: INSERT
  public function insert($table, $data) {
    self::dbAccess();
    $result = array();
    $columns = array();
    $values = array();
    foreach ($data as $column => $value) {
        $columns[] = $column;
        $values[] = $value;
    }
    $columns_imploded = implode(",", $columns);
    $values_imploded = "'" . implode("','", $values) . "'";
    $query = "INSERT INTO  $table ($columns_imploded) VALUES($values_imploded) ";
    $DataSource = DB::query($query);
  }

  //SQL COMMAND: REPLACE
  public function replace($table, $data) {
    self::dbAccess();
    $result = array();
    $columns = array();
    $values = array();
    foreach ($data as $column => $value) {
        $columns[] = $column;
        $values[] = $value;
    }
    $columns_imploded = implode(",", $columns);
    $values_imploded = "'" . implode("','", $values) . "'";
    $query = "REPLACE INTO  $table ($columns_imploded) VALUES($values_imploded) ";
    $DataSource = DB::query($query);
  }

  //SQL COMMAND: DELETE 
  public function delete($table, $field, $id) {
    self::dbAccess();
    $query = "DELETE FROM $table WHERE $field = $id";
    $DataSource = DB::query($query);
  }

  //SQL COMMAND: RUN ANY SQL COMMAND CUSTOM 
  // public function rawSQL($SQL_COMMANDS) {
  //   self::dbAccess();
  //   $query = $SQL_COMMANDS;
  //   $DataSource = DB::query($query);
  // }

  //SQL COMMAND: FOUR COLUMN TABLE 
  public function createFourColumnTable($table, $data_one, $data_two, $data_three, $data_four) {
    self::dbAccess();
    $query = "CREATE TABLE IF NOT EXISTS $table (
      $data_one bigint(20) NOT NULL AUTO_INCREMENT, 
      $data_two varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
      $data_three varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
      $data_four varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      PRIMARY KEY ($data_one))";
    $DataSource = DB::query($query);
  }
  
}
?>