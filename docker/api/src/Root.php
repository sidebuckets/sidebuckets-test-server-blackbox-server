<?php 
namespace BlackBox\Components;
use BlackBox\Components\Queries;

require_once __DIR__ . '/DataSource.php';

interface Resolver {
    public function resolve($rootValue, $args, $context);
}
class createEvent implements Resolver 
{
  public function resolve($rootValue, $args, $context) {
    $event = array("_id" => rand(1, 40), "title" => $args['title'], "price" => (float)$args['price'], "description" => $args['description'], "date" => date(DATE_ATOM,mktime(0,0,0,10,3,1975)));
    return $event;
  }
}
class createUser implements Resolver 
{
  public function dbCreate($submit = []) {

    // Uses INSERT method for mysql 
    return DataSource::create($submit['user_name'], $submit['user_email'], $submit['user_password']);
  }
  public function resolve($rootValue, $args, $context) {
    return self::dbCreate(array("user_name" => $args['user_name'], "user_email" => $args['user_email'], "user_password" => $args['user_password']));
  }
}
class updateUser implements Resolver 
{
  public function dbUpdate($submit = []) {
   
    // Users REPLACE method for myql
    return DataSource::put($submit['user_id'], $submit['user_name'], $submit['user_email'], $submit['user_password']);
  }
  public function resolve($rootValue, $args, $context) 
  {
    return self::dbUpdate(array("user_id" => $args['user_id'], "user_name" => $args['user_name'], "user_email" => $args['user_email'], "user_password" => $args['user_password']));
  }
}
class deleteUser implements Resolver 
{
  public function dbDelete($submit = []) {
  
    // Users DELETE method for myql
    return DataSource::remove($submit['user_id']);
  }
  public function resolve($rootValue, $args, $context) 
  {
    return self::dbDelete(array("user_id" => $args['user_id']));
  }
}
class events implements Resolver 
{
  public function HardCodeDB()
  {
    # Hardcoded DB
    return array(
      1 => array("_id" => "1", "title"  => "testing", "description" => "tested", "date"=> "111"), 
      2 => array("_id" => "2", "title"  => "Two", "description" => "tested", "date"=> "222"),
      3 => array("_id" => "3", "title"  => "Three", "description" => "tested Online", "date"=> "456")
    );
  }
   public function resolve($rootValue, $args, $context)
    {
        $sampleData = self::HardCodeDB();
        return $sampleData;
    }
}
class users implements Resolver 
{

  public function HardCodeDB()
  {
    # Hardcoded DB
    return array(
      0 => array("user_id" => "1", "user_name"  => "tested", "user_email" => "tested", "user_password"=> "111")
    );
  }

  public function resolve($rootValue, $args, $context)
  {
    return DataSource::get();
  }
}
return [
    'createEvent' => function($rootValue, $args, $context) {
        $create = new createEvent();
        return $create->resolve($rootValue, $args, $context);
    },
    'events' => function($rootValue, $args, $context) {
        $find = new events();
        return $find->resolve($rootValue, $args, $context);
    },
    'createUser' => function($rootValue, $args, $context) {
        $create = new createUser();
        return $create->resolve($rootValue, $args, $context);
    },
    'user' => function($rootValue, $args, $context) {
        $locate = new users();
        return $locate->resolve($rootValue, $args, $context);
    },
    'updateUser' => function($rootValue, $args, $context) {
        $run = new updateUser();
        return $run->resolve($rootValue, $args, $context);
    },
    'deleteUser' => function($rootValue, $args, $context) {
        $delete = new deleteUser();
        return $delete->resolve($rootValue, $args, $context);
    }
];
?>