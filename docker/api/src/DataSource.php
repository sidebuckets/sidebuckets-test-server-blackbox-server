<?php 
namespace BlackBox\Components;

require_once __DIR__ . '/SQL.php';

class DataSource {
  
  public static $user     = '';
  public static $password = '';
  public static $dbName   = '';
  public static $dbhost   = '';
  public static $submit   = [];
  
  private function dbAccess() {
    DB::$user     = 'user';
    DB::$password = 'password';
    DB::$dbName   = 'db';
    DB::$host     = 'mysql:3306';
  }
  
  //API COMMAND: GET USERS
  public function get() {
    self::dbAccess();
    $DataSource = DB::query("SELECT * FROM tbl_user");
    $container = [];
    while($row = mysqli_fetch_assoc($DataSource))
      $container[] = $row; 
      // Sample Data OutPut: 
      //  return [
      //             '1' => ([
      //                 'user_id' => '1',
      //                 'user_email' => 'john@example.com',
      //                 'user_name' => 'John C',
      //                 'user_password' => 'Doe D'
      //             ]),
      //         ];
    return $container;
  }
  //API COMMAND: CREATE USER  
  public function create($user_name, $user_email, $user_password) {
    self::dbAccess();

    //VALIDATION 

    //Test Table Creation function 
    $test = SQL::createFourColumnTable('tbl_user_test', 'user_id', 'user_fullname', 'user_email', 'user_passwordtest');

      //Check if user name exists, user email exists or password exists 
      //Use sql raw commands to create validation 
      // $validation = SQL::rawSQL("SELECT ");

    $userData = array(
      "user_name"     => $user_name,
      "user_email"    => $user_email,
      "user_password" => password_hash($user_password, PASSWORD_DEFAULT)
    );
    //SQL COMMAND: INSERT
    SQL::insert('tbl_user', $userData); 
   
    return $userData;

  }
  //API COMMAND: UPDATE USER  
  public function put($user_id, $user_name, $user_email, $user_password) {
    self::dbAccess();
    $userData = array(
      "user_id"       => $user_id,
      "user_name"     => $user_name,
      "user_email"    => $user_email,
      "user_password" => password_hash($user_password, PASSWORD_DEFAULT)
    );
    //SQL COMMAND: REPLACE
    SQL::replace('tbl_user', $userData); 
   
    return $userData;

  }
  //API COMMAND: REMOVE USER  
  public function remove($user_id) {
    self::dbAccess();
    $userData = array(
      "table"   => "tbl_user",
      "field"   => "user_id",
      "user_id" => $user_id
    );
    //SQL COMMAND: DELETE
    SQL::delete($userData['table'], $userData['field'], $userData['user_id']); 
   
    return $userData;
  }

}

?>